# Imports the Google Cloud client library
from google.cloud import speech_v1p1beta1 as speech

# import os library to get yaml file variables
import os
import datetime


def wisor_transcribe(event, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    file = event

    # Instantiates a client
    client = speech.SpeechClient()

    print(f"Transcribing File: {file['name']}.")
    print('Event ID: {}'.format(context.event_id))
    print('Event type: {}'.format(context.event_type))
    print('Bucket: {}'.format(event['bucket']))
    print('File: {}'.format(event['name']))
    print('Metageneration: {}'.format(event['metageneration']))
    print('Created: {}'.format(event['timeCreated']))
    print('Updated: {}'.format(event['updated']))

    gcs_uri = 'gs://' + event['bucket'] + '/' + event['name']

    audio = speech.RecognitionAudio(uri=gcs_uri)

    config = set_recognition_config()

    output_config = set_output_config(event)

    operation = client.long_running_recognize(
        request={"config": config, "audio": audio, 'output_config': output_config})

    # TODO Put in better error logging and handling
    print("File sent to transcription.   ID: " + operation.operation.name)


##############
# Consume the yaml file environment variables related to audio encoding of being transcribed
##############
def set_recognition_config():
    # Encoding of audio data sent. This sample sets this explicitly.
    encoding = os.getenv('TRANSCRIPTION-ENCODING')
    if encoding == "LINEAR16" or encoding == "WAV":
        encoding = speech.RecognitionConfig.AudioEncoding.LINEAR16
    elif encoding == "FLAC":
        encoding = speech.RecognitionConfig.AudioEncoding.FLAC
    else:
        encoding = speech.RecognitionConfig.AudioEncoding.MP3

    # Sample rate in Hertz of the audio data sent
    # TODO:   log an error if this isn't an integer
    sample_rate_hertz = int(os.getenv('TRANSCRIPTION-SAMPLE-RATE', 44100))

    # The language of the supplied audio
    language_code = os.getenv('TRANSCRIPTION-LANGUAGE-CODE', "en-US")

    # Number of speakers
    speaker_count = int(os.getenv('TRANSCRIPTION-SPEAKER-COUNT', 1))
    print("Number of speakers: {}".format(speaker_count))
    if speaker_count == 1:
        enable_diarization = False
    else:
        enable_diarization = True

    # Use Enhanced Model
    enhanced_model = int(os.getenv('TRANSCRIPTION-ENHANCED-MODEL', 1))
    print("Number of speakers: {}".format(speaker_count))
    if enhanced_model == 1:
        enhanced_model = True
    else:
        enhanced_model = False

    # Transcription model
    model = os.getenv('TRANSCRIPTION-MODEL', "default")

    return speech.RecognitionConfig({'encoding': encoding, 'sample_rate_hertz': sample_rate_hertz,
                                     'language_code': language_code,
                                     'enable_speaker_diarization': enable_diarization,
                                     'diarization_speaker_count': speaker_count,
                                     "use_enhanced": enhanced_model,
                                     'enable_automatic_punctuation': True,})


##############
# Build the configuration of where the raw transcript will be written
# and name of the output file
#
# This will be another google cloud storage bucket in the same account that triggers this function
##############
def set_output_config(event):
    # get bucket name from environment variable or default to add "-transcripts" to the calling bucket name
    bucket_name = os.getenv('STORAGE-TRANSCRIPTION-BUCKET')
    if not bucket_name:
        bucket_name = event['bucket'] + '-transcript'

    # output filename will include generated timestamp and original file name
    timestamp = datetime.datetime.now()
    timestamp = timestamp.strftime("%Y-%m-%d-%H-%M-%S-")
    file_name = timestamp + event['name'] + '.json'

    output_uri = 'gs://' + bucket_name + '/' + file_name

    print('Output path: ' + output_uri)

    return speech.types.TranscriptOutputConfig(gcs_uri=output_uri)
