# WiSOR GCP Transcribe

Wisconsin Surgical Outcomes Research (WiSOR) Speech to Text full cloud implementation (as opposed to command line hybrid implementation)

# wisor-transcribe
Google Function that will be connected to a Cloud Storage bucket and fire each time a file is uploaded to the folder

More information about Google Cloud Storage Triggers:
https://cloud.google.com/functions/docs/calling/storage

## Configuration file
The configuration file (.env.yaml) contains a number of variables that can be configured for different types of files, different types of transcription, and different targets.

- The .env.yaml file inserts values into the Cloud Function Enviornment Variables  
- To change the value of these variables, either edit the yaml file and redeploy the function (see command line deployment, below) or edit them in the Cloud Console UI described in the enviornment variables documentation:  https://cloud.google.com/functions/docs/env-var

## Command line deployment
Requires installation of the Google Cloud SDK: https://cloud.google.com/sdk/docs/install

Call this gcloud command in the same directory as the source code, having already 

```
gcloud functions deploy wisor_transcribe  --runtime python38 --trigger-resource REPLACE-WITH-BUCKET-NAME  --trigger-event google.storage.object.finalize --env-vars-file=.env.yaml
```
- Note handle to point to the configuration file 
- Be sure to insert the name of the bucket that source files are uploaded to, which are connecting the function to in the command line call.

# Transcription Testing (invoking the speech to text API without using the cloud function)

## Example Command Line (gcloud) for Testing
https://cloud.google.com/speech-to-text/docs/quickstart-gcloud

```
gcloud ml speech recognize gs://cloud-samples-tests/speech/brooklyn.flac --language-code=en-US
```

## Example REST API Call for Testing
```
curl -X POST     -H "Authorization: Bearer "$(gcloud auth application-default print-access-token)     -H "Content-Type: application/json; charset=utf-8"     --data "{
  'config': {'encoding': 'MP3', 'sampleRateHertz': 44100,'languageCode': 'en-US'},
  'output_config': {
    'gcs_uri':'gs://REPLACE-WITH-OUTPUT-BUCKET-NAME/AP_Pilot_Interview2_11-10-20.txt'
  },
  'audio': {
    'uri': 'gs://REPLACE-WITH-BUCKET-NAME/AP_Pilot_Interview2_11-10-20.mp4'
  }
 }" "https://speech.googleapis.com/v1p1beta1/speech:longrunningrecognize"
 ```
 ### Polling Response
```
gcloud ml speech operations describe 479168289094830004
```
 where the number is the ID returned from the call to the API, above.

 This number can also be obtained by viewing the function "logs" tab, in the cloud console UI function details.
