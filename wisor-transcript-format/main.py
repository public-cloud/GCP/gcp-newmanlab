# import os library to get yaml file variables
import os
import json

#import google cloud storage to access file contents
from google.cloud import storage

def wisor_transcript_format(event, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    file = event
    storage_client = storage.Client()

    print(f"Formatting Transcription File: {file['name']}.")
    print('Event ID: {}'.format(context.event_id))
    print('Event type: {}'.format(context.event_type))
    print('Bucket: {}'.format(event['bucket']))
    print('File: {}'.format(event['name']))
    print('Metageneration: {}'.format(event['metageneration']))
    print('Created: {}'.format(event['timeCreated']))
    print('Updated: {}'.format(event['updated']))

    bucket = storage_client.get_bucket(file['bucket'])
    blob = bucket.blob(file['name'])

    # TODO:   handle an empty file gracefully
    data = json.loads(blob.download_as_string(client=None))
    # appears to be a result for each exchange of speaker
    for result in data["results"]:
        # generally one alternative per result
        transcript_phrase = result["alternatives"]
        print("Confidence: {}".format(transcript_phrase["confidence"]))

    # get bucket name from environment variable or default to add "-transcripts" to the calling bucket name
    bucket_name = os.getenv('STORAGE-TRANSCRIPTION-FORMATTED-BUCKET')
    if not bucket_name:
        bucket_name = event['bucket'] + '-formatted'

    # output filename will include original file name
    file_name = event['name'] + ".txt"

    output_uri = 'gs://' + bucket_name + '/' + file_name

    print('Output path: ' + output_uri)
